
#! /bin/bash


# install stuff
sudo dnf -y install nano


# office
sudo dnf -y install libreoffice


#mail
sudo dnf -y install evolution evolution-ews


# install brave
sudo dnf -y install dnf-plugins-core
sudo dnf config-manager --add-repo https://brave-browser-rpm-release.s3.brave.com/x86_64/
sudo rpm --import https://brave-browser-rpm-release.s3.brave.com/brave-core.asc
sudo dnf -y install brave-browser




# move to temp directory so all downloaded files will be removed
cd /tmp

# folder removal at start
cat > rc.local << EOF
#! /bin/bash

rm -rf /home/user/QubesIncoming
rm -rf /home/user/Downloads/*
EOF

sudo cp rc.local /etc/rc.d/
sudo chmod +x /etc/rc.d/rc.local


# get new hosts file from someonewhocares.org
wget https://someonewhocares.org/hosts/hosts
sudo cp hosts /etc/hosts

